//#pragma once

#ifndef e
#define e


class RandomNumberGenerator {
public:
	RandomNumberGenerator();
	int getRandomValue(int) const;
private:
	void seed() const;
};


#endif e